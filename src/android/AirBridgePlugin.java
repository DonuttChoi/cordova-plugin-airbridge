package io.airbridge.cordova;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.airbridge.routing.DeepLinkHandler;
import io.airbridge.AirBridge;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.content.Context;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * This class echoes a string called from JavaScript.
 */
public class AirBridgePlugin extends CordovaPlugin {

    public class RunnableWithArgs implements Runnable {
        private String action;
        private JSONArray args;
        private CallbackContext callbackContext;

        public RunnableWithArgs(String _action,JSONArray _args, CallbackContext _callbackContext) {
            this.action = _action;
            this.args = _args;
            this.callbackContext = _callbackContext;
        }

        public void run() {
            try {
                if (action.equals("initInstance")) {
                    initInstance(args.getString(0), args.getString(1), callbackContext);
                } else if (action.equals("goal")) {
                    goal(args, callbackContext);
                } else if (action.equals("userSignup")) {
                    userSignup(args.getString(0), callbackContext);
                } else if (action.equals("userSignin")) {
                    userSignin(args.getString(0), callbackContext);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        Runnable t = new RunnableWithArgs(action,args,callbackContext);
        
        if (action.equals("initInstance")) {
            cordova.getThreadPool().execute(t);
            return true;
        } else if (action.equals("goal")) {
            cordova.getThreadPool().execute(t);
            return true;
        } else if (action.equals("userSignup")) {
            cordova.getThreadPool().execute(t);
            return true;
        } else if (action.equals("userSignin")) {
            cordova.getThreadPool().execute(t);
            return true;
        }  
        return false;
    }

    private void initInstance(String userToken, String appToken, CallbackContext callbackContext) {
        try{
            AirBridge.setDebugMode(true);
        
            AirBridge.initCordovaSDK(webView.getContext(), 
                appToken, 
                userToken
            );
            
            AirBridge.noticeSdkInstallFrom3rdPartySDK("Android_Cordova_0.1.3","A");
            
            final CallbackContext savedCallbackContext = callbackContext;
            AirBridge.addSimpleLinkHandler(new DeepLinkHandler() {
                @Override
                public void onLink(String url, Bundle _data, Context context) {
                    _data.putString("routing",url);
                    
                    JSONObject json = new JSONObject();
                    Set<String> keys = _data.keySet();
                    for (String key : keys) {
                        try {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                json.put(key, JSONObject.wrap(_data.get(key)));
                            } else {
                                json.put(key, AirBridgePlugin.wrap(_data.get(key)));
                            }
                        } catch(JSONException e) {
                            // pass
                        } catch (Exception e) {
                            // pass
                        }
                    }
                    PluginResult dataResult = new PluginResult(PluginResult.Status.OK, json);
                    dataResult.setKeepCallback(true);
                    savedCallbackContext.sendPluginResult(dataResult);
                }
            });
        } catch(Exception e) {
            callbackContext.error("SDK error occurs:");
            e.printStackTrace();
        }
    }
    
    private void goal(JSONArray args, CallbackContext callbackContext) throws JSONException {
        String goalDescription = "";
        String goalCategory = "";

        if (args.length() > 0) {
            goalDescription = args.getString(0);
        }
        if (args.length() >1) {
            goalCategory = args.getString(1);
        }
        
        AirBridge.goal(goalDescription,goalCategory);
        
        callbackContext.success();
    }

    private void userSignup(String value, CallbackContext callbackContext) {
        AirBridge.userSignup(value);
        callbackContext.success();
    }

    private void userSignin(String value, CallbackContext callbackContext) {
        AirBridge.userSignin(value);
        callbackContext.success();
    }
    /*
        wrap method & toJSONArray method from JSONObject.java
     */
    public static Object wrap(Object o) {
        if (o == null) {
            return JSONObject.NULL;
        }
        if (o instanceof JSONArray || o instanceof JSONObject) {
            return o;
        }
        if (o.equals(JSONObject.NULL)) {
            return o;
        }
        try {
            if (o instanceof Collection) {
                return new JSONArray((Collection) o);
            } else if (o.getClass().isArray()) {
                return toJSONArray(o);
            }
            if (o instanceof Map) {
                return new JSONObject((Map) o);
            }
            if (o instanceof Boolean ||
                    o instanceof Byte ||
                    o instanceof Character ||
                    o instanceof Double ||
                    o instanceof Float ||
                    o instanceof Integer ||
                    o instanceof Long ||
                    o instanceof Short ||
                    o instanceof String) {
                return o;
            }
            if (o.getClass().getPackage().getName().startsWith("java.")) {
                return o.toString();
            }
        } catch (Exception ignored) {
        }
        return null;
    }

    public static JSONArray toJSONArray(Object array) throws JSONException {
        JSONArray result = new JSONArray();
        if (!array.getClass().isArray()) {
            throw new JSONException("Not a primitive array: " + array.getClass());
        }
        final int length = Array.getLength(array);
        for (int i = 0; i < length; ++i) {
            result.put(wrap(Array.get(array, i)));
        }
        return result;
    }

}